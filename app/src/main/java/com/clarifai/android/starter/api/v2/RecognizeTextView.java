package com.clarifai.android.starter.api.v2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.clarifai.android.starter.api.v2.activity.BaseActivity;
import com.clarifai.android.starter.api.v2.activity.ColorSelectActivity;
import com.clarifai.android.starter.api.v2.activity.RecognizeCompTaskActivity;
import com.clarifai.android.starter.api.v2.activity.RecognizeTaskActivity;
import com.clarifai.android.starter.api.v2.adapter.PredictionResultsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import clarifai2.dto.model.Model;
import clarifai2.dto.prediction.Prediction;


/**
 * A view that recognizes using the given {@link Model} and displays the image that was recognized
 * upon, along with the list of recognitions for that image
 */
public class RecognizeTextView extends CoordinatorLayout implements HandlesPickImageIntent{


  @BindView(R.id.imageView)
  ImageView imageView;

  @BindView(R.id.fab)
  View fab;

  @BindView((R.id.textView))
  TextView Text;




  public RecognizeTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflate(context, R.layout.view_set_text, this);
    ButterKnife.bind(this);

  }

  public void setLayout(Context context, int layout){
    inflate(context, layout, this);
    ButterKnife.bind(this);

  }



  @OnClick(R.id.fab)
  void selectImageToUpload() {
    Intent placeholder = new Intent(getContext(), ColorSelectActivity.class);
    ClarifaiUtil.unwrapActivity(getContext())
            .startActivity(placeholder);

    //ClarifaiUtil.unwrapActivity(getContext())
    //    .startActivity(new Intent(getContext(), RecognizeTaskActivity.class));
  }

  private void setBusy(final boolean busy) {
    ClarifaiUtil.unwrapActivity(getContext()).runOnUiThread(new Runnable() {
      @Override
      public void run() {
        imageView.setVisibility(busy ? GONE : VISIBLE);
        fab.setEnabled(!busy);
      }
    });
  }
  public void setAltText(String text){
    Text.setText(text);
  }
}
