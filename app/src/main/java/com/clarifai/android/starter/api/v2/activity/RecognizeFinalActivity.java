package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeFinalView;
import com.clarifai.android.starter.api.v2.RecognizeTextView;

import butterknife.ButterKnife;
import capturekingdom.Tasks;

public class RecognizeFinalActivity extends BaseActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    Log.d("On Create", "Launched Final");
    super.tasks = Tasks.getTasks();
    super.onCreate(savedInstanceState);
    byte[] value = Color.image;
    if(mp == null || !mp.isPlaying()) {
      mp = MediaPlayer.create(this, R.raw.opening_song);
      mp.setLooping(true);
      mp.start();
    }



    ButterKnife.<RecognizeFinalView>findById(this, R.id.content_root)
            .setAltText(getApplicationContext().getString(R.string.opening));

   // ButterKnife.<RecognizeFinalView>findById(this, R.id.content_root)
   //         .setImageGroup(value);

    ButterKnife.<RecognizeFinalView>findById(this, R.id.content_root)
            .setImageCastle();


  }

  @Override
  protected int layoutRes() {
    return R.layout.activity_final_recognize;
  }

  @Override
  protected void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();
  }
}
