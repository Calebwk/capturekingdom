package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeTaskView;

import butterknife.ButterKnife;
import capturekingdom.Tasks;
import clarifai2.dto.prediction.Concept;

public class RecognizeTaskActivity extends BaseActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.tasks = Tasks.getTasks();
    Bundle b = getIntent().getExtras();
    int value = -1; // or other values
    if(b != null)
      value = b.getInt("Offset");
    super.task_count = value;
    Log.d("task offset", Integer.toString(value));

    super.onCreate(savedInstanceState);


    RecognizeTaskView id;
    ButterKnife.<RecognizeTaskView<Concept>>findById(this, R.id.content_root).setAltText(super.alt_text);
  }

  @Override
  protected int layoutRes() {
    return R.layout.activity_task_recognize;
  }

  @Override
  public void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();
  }
}
