package com.clarifai.android.starter.api.v2;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.app.Activity;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import capturekingdom.ImageTask;
import capturekingdom.StartsWithTask;
import clarifai2.api.ClarifaiResponse;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.input.image.ClarifaiImage;
import clarifai2.dto.model.Model;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Prediction;
import com.clarifai.android.starter.api.v2.activity.BaseActivity;
import com.clarifai.android.starter.api.v2.adapter.PredictionResultsAdapter;

import org.w3c.dom.Text;

import timber.log.Timber;

import java.util.List;


/**
 * A view that recognizes using the given {@link clarifai2.dto.model.Model} and displays the image that was recognized
 * upon, along with the list of recognitions for that image
 */
public class RecognizeTaskView<PREDICTION extends Prediction> extends CoordinatorLayout implements HandlesPickImageIntent{


  @BindView(R.id.imageView)
  ImageView imageView;

  @BindView(R.id.fab)
  View fab;

  @BindView((R.id.TextView))
  TextView Text;


  @NonNull
  private final PredictionResultsAdapter<PREDICTION> tagsAdapter;

  public RecognizeTaskView(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflate(context, R.layout.view_task, this);
    ButterKnife.bind(this);

    tagsAdapter = new PredictionResultsAdapter<>();
  }

  public void setLayout(Context context, int layout){
    inflate(context, layout, this);
    ButterKnife.bind(this);

  }



  @OnClick(R.id.fab)
  void selectImageToUpload() {

    ClarifaiUtil.unwrapActivity(getContext())
        .startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), BaseActivity.TAKE_IMAGE);
  }

  private void setBusy(final boolean busy) {
    ClarifaiUtil.unwrapActivity(getContext()).runOnUiThread(new Runnable() {
      @Override
      public void run() {
        imageView.setVisibility(busy ? GONE : VISIBLE);
        fab.setEnabled(!busy);
      }
    });
  }
  public void setAltText(String text){
    Text.setText(text);
  }
}
