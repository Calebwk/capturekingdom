package com.clarifai.android.starter.api.v2.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;

import com.clarifai.android.starter.api.v2.ClarifaiUtil;
import com.clarifai.android.starter.api.v2.HandlesPickImageIntent;
import com.clarifai.android.starter.api.v2.R;

import java.io.ByteArrayOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import capturekingdom.ColorRecordTask;
import capturekingdom.ColorTask;
import capturekingdom.ImageTask;



public abstract class BaseActivity extends AppCompatActivity {

  @BindView(R.id.content_root)
  public View root;
  public int task_count = -1;
  public byte[] image;


  public static final int TAKE_IMAGE = 100;

  private static final String INTENT_EXTRA_DRAWER_POSITION = "IntentExtraDrawerPosition";

  protected MediaPlayer mp;

  private Unbinder unbinder;
  public ImageTask Task;
  public ImageTask[] tasks;
  public String alt_text;
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    nextTask();
    @SuppressLint("InflateParams") final View wrapper = getLayoutInflater().inflate(R.layout.activity_wrapper, null, true);
    final ViewStub stub = ButterKnife.findById(wrapper, R.id.content_stub);


    stub.setLayoutResource(layoutRes());
    stub.inflate();
    setContentView(wrapper);
    unbinder = ButterKnife.bind(this);

    final Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);


    // Show the "hamburger"
    setSupportActionBar(toolbar);
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(false);
    }
    getSupportActionBar().hide();
  }


  public void nextTask(){
    task_count++;
    if (task_count == tasks.length){
      task_count=0;
      Log.d("Last TIme", "Done!");
      Intent placeholder = new Intent(this, RecognizeFinalActivity.class);
      Bundle b = new Bundle();
      Log.d("Length", Integer.toString(Color.image.length));
      b.putByteArray("image", Color.image);
      placeholder.putExtras(b);
      this.startActivity(placeholder);
    }
    Task = tasks[task_count];
    Log.d("alt text", Task.getAdviserText(this));
    alt_text = Task.getAdviserText(this);
    final List<HandlesPickImageIntent> imageHandlers = ClarifaiUtil.childrenOfType(root, HandlesPickImageIntent.class);
    for (HandlesPickImageIntent imageHandler : imageHandlers) {
      imageHandler.setAltText(Task.getAdviserText(this));
    }
  }

  @Override
  protected void onDestroy() {
    unbinder.unbind();
    super.onDestroy();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
      this.onImagePicked(retrieveSelectedImage(requestCode, resultCode, data), Task);
    }



  @LayoutRes
  protected abstract int layoutRes();


  @Nullable
  protected final byte[] retrieveSelectedImage(int requestCode, int resultCode, Intent data) {
    if (resultCode != RESULT_OK || requestCode != TAKE_IMAGE) {
      return null;
    }
    Bitmap bitmap = (Bitmap) data.getExtras().get("data"); //BitmapFactory.decodeStream(inStream);
    final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
    bitmap.recycle();
    return outStream.toByteArray();
  }

  private Boolean goToActivityListener(@NonNull final Class<? extends Activity> activityClass) {
        if (BaseActivity.this.getClass().equals(activityClass)) {
         return false;
        }
        startActivity(new Intent(BaseActivity.this, activityClass));
        return true;
      }


  public void onImagePicked(@Nullable final byte[] imageBytes, final ImageTask Task) {
    if (imageBytes == null) {
      return;
    }
    Color.image = imageBytes;
    new AsyncTask<Void, Void, Boolean>() {
      @Override
      protected Boolean doInBackground(Void... params) {


        return Task.isValidImage(imageBytes);


      }

      @Override
      protected void onPostExecute(Boolean Result) {

        if (Task instanceof ColorRecordTask){
          Color.color = ((ColorRecordTask) Task).getColor();
          Log.d("Color", Integer.toString(Color.color));

        }

        if (Result) {

          Snackbar.make(findViewById(R.id.content_root),
                  "It Worked!!!",
                  Snackbar.LENGTH_LONG).show();
          Intent placeholder = new Intent(BaseActivity.this, RecognizeCompTaskActivity.class);
          Bundle b = new Bundle();
          b.putInt("Offset", BaseActivity.this.task_count);
          placeholder.putExtras(b);
          BaseActivity.this.startActivity(placeholder);
        }
        else {
          Snackbar.make(
                  findViewById(R.id.content_root),
                 "Wrong Image!",
                  Snackbar.LENGTH_LONG
          ).show();
        }

      }
  }.execute();

}

}
