package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.ClarifaiUtil;
import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeCompTaskView;

import java.util.List;

import butterknife.ButterKnife;
import capturekingdom.ColorTask;
import capturekingdom.ImageTask;
import clarifai2.dto.prediction.Concept;

public class RecognizeCompTaskActivity extends BaseActivity {
  public static ImageTask blueThingTask = new ColorTask(0xFF0000FF, "blue", .1, "Please take a picture of blue");
  public static ImageTask[] Tasks = {blueThingTask};
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.tasks = Tasks;
    Bundle b = getIntent().getExtras();
    int value = -1; // or other values
    if(b != null)
      value = b.getInt("Offset");

    super.onCreate(savedInstanceState);



    Log.d("Offset", Integer.toString(value));
    RecognizeCompTaskView id;
    ButterKnife.<RecognizeCompTaskView<Concept>>findById(this, R.id.content_root).setAltText(getResources().getStringArray(R.array.task_passed)[(int)(Math.random()*4)]);
    ButterKnife.<RecognizeCompTaskView<Concept>>findById(this, R.id.content_root).setOffset(value);

    final List<RecognizeTaskActivity> list = ClarifaiUtil.childrenOfType(super.root, RecognizeTaskActivity.class);
    Log.d("RecognizeTaskActivities", Integer.toString(list.size()));
    for(RecognizeTaskActivity item : list){
    }
  }


  @Override
  protected int layoutRes() {
    return R.layout.activity_comptask_recognize;
  }

  @Override
  public void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();
  }
}
