package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.ColorSelectView;
import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeTextView;

import butterknife.ButterKnife;
import capturekingdom.ColorRecordTask;
import capturekingdom.ColorTask;
import capturekingdom.ImageTask;

public class ColorSelectActivity extends BaseActivity {
  public static ImageTask blueThingTask = new ColorRecordTask(0xFF0000FF, "blue", .1, "Please take a picture of blue");
  public static ImageTask[] Tasks = {blueThingTask};
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.tasks = Tasks;
    super.onCreate(savedInstanceState);

    ColorSelectView id;
    id = ButterKnife.<ColorSelectView>findById(this, R.id.content_root);

    mp = MediaPlayer.create(this, R.raw.opening_song);
    mp.start();



    ButterKnife.<ColorSelectView>findById(this, R.id.content_root).setAltText
            ("Take a picture of something vibrant to define the color of your castle!");


  }

  @Override
  protected int layoutRes() {
    return R.layout.activity_color_select_recognize;
  }

  @Override
  protected void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();

  }

}
