package com.clarifai.android.starter.api.v2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.clarifai.android.starter.api.v2.activity.Color;
import com.clarifai.android.starter.api.v2.activity.ColorSelectActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import capturekingdom.CKUtils;
import clarifai2.dto.model.Model;


/**
 * A view that recognizes using the given {@link Model} and displays the image that was recognized
 * upon, along with the list of recognitions for that image
 */
public class RecognizeFinalView extends CoordinatorLayout implements HandlesPickImageIntent{


  @BindView(R.id.fab)
  View fab;

  @BindView((R.id.textView))
  TextView Text;

  @BindView(R.id.imageGroup)
  ImageView ImageGroup;

  @BindView(R.id.imageCastle)
  ImageView ImageCastle;

  Bitmap Castle;


  public RecognizeFinalView(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflate(context, R.layout.view_final, this);
    ButterKnife.bind(this);

  }

  public void setLayout(Context context, int layout){
    inflate(context, layout, this);
    ButterKnife.bind(this);

  }

  public void setImageGroup(byte[] image){
    ImageGroup.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
  }

  public void setImageCastle() {
    Castle = BitmapFactory.decodeResource(ClarifaiUtil.unwrapActivity(getContext()).getResources(), R.mipmap.castle_template);

    new AsyncTask<Void, Void, Bitmap>() {
      @Override
      protected Bitmap doInBackground(Void... params) {


        Log.d("Castle Len", Integer.toString(Castle.getByteCount()));

        CKUtils utils = new CKUtils();
        return utils.getCastle(Castle, Color.color);
      }

      @Override
      protected void onPostExecute(Bitmap Result) {
        ImageCastle.setImageBitmap(Result);


      }
    }.execute();
  }

  @OnClick(R.id.fab)
  void selectImageToUpload() {
    ClarifaiUtil.unwrapActivity(getContext()).finishAffinity();

    //ClarifaiUtil.unwrapActivity(getContext())
    //    .startActivity(new Intent(getContext(), RecognizeTaskActivity.class));
  }

  private void setBusy(final boolean busy) {
    ClarifaiUtil.unwrapActivity(getContext()).runOnUiThread(new Runnable() {
      @Override
      public void run() {
        fab.setEnabled(!busy);
      }
    });
  }
  public void setAltText(String text){
    Text.setText(text);
  }
}
