package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeTaskView;

import butterknife.ButterKnife;
import clarifai2.dto.prediction.Color;

public class RecognizeColorsActivity extends BaseActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    RecognizeTaskView id;
    id = ButterKnife.<RecognizeTaskView<Color>>findById(this, R.id.content_root);
  }

  @Override
  protected int layoutRes() {
    return R.layout.activity_task_recognize;
  }

  @Override
  protected void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(10600);
        } catch (Exception e) {
          animation.stop();
        }
        animation.stop();
      }
    }).start();
  }
}
