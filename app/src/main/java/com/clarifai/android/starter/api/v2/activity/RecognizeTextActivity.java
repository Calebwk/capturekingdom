package com.clarifai.android.starter.api.v2.activity;

import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.clarifai.android.starter.api.v2.R;
import com.clarifai.android.starter.api.v2.RecognizeTextView;

import butterknife.ButterKnife;
import capturekingdom.ColorTask;
import capturekingdom.ImageTask;
import capturekingdom.Tasks;

public class RecognizeTextActivity extends BaseActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.tasks = Tasks.getTasks();
    super.onCreate(savedInstanceState);

    RecognizeTextView id;
    id = ButterKnife.<RecognizeTextView>findById(this, R.id.content_root);

    if(mp == null || !mp.isPlaying()) {
      mp = MediaPlayer.create(this, R.raw.opening_song);
      mp.setLooping(true);
      mp.start();
    }



    ButterKnife.<RecognizeTextView>findById(this, R.id.content_root)
            .setAltText(getApplicationContext().getString(R.string.opening));


  }

  @Override
  protected int layoutRes() {
    return R.layout.activity_text_recognize;
  }

  @Override
  protected void onResume() {
    super.onResume();
    ImageView view = (ImageView) findViewById(R.id.imageView);
    final AnimationDrawable animation = (AnimationDrawable) view.getDrawable();

    animation.start();
  }
}
