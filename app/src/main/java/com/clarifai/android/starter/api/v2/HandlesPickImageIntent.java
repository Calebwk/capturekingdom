package com.clarifai.android.starter.api.v2;


import android.content.Context;
import android.util.AttributeSet;

public interface HandlesPickImageIntent {
  void setAltText(String text);
  void setLayout(Context context, int layout);
}
