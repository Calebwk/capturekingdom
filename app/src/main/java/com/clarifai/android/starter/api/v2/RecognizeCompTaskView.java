package com.clarifai.android.starter.api.v2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.clarifai.android.starter.api.v2.activity.BaseActivity;
import com.clarifai.android.starter.api.v2.activity.RecognizeCompTaskActivity;
import com.clarifai.android.starter.api.v2.activity.RecognizeTaskActivity;
import com.clarifai.android.starter.api.v2.adapter.PredictionResultsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import clarifai2.dto.model.Model;
import clarifai2.dto.prediction.Prediction;


/**
 * A view that recognizes using the given {@link Model} and displays the image that was recognized
 * upon, along with the list of recognitions for that image
 */
public class RecognizeCompTaskView<PREDICTION extends Prediction> extends CoordinatorLayout implements HandlesPickImageIntent{




  @BindView(R.id.fab)
  View fab;

  @BindView((R.id.textView))
  TextView Text;

  public int offset;

  @NonNull
  private final PredictionResultsAdapter<PREDICTION> tagsAdapter;

  public RecognizeCompTaskView(Context context, AttributeSet attrs) {
    super(context, attrs);
    inflate(context, R.layout.view_task_complete, this);
    ButterKnife.bind(this);

    tagsAdapter = new PredictionResultsAdapter<>();
  }

  public void setLayout(Context context, int layout){
    inflate(context, layout, this);
    ButterKnife.bind(this);

  }



  @OnClick(R.id.fab)
  void selectImageToUpload() {
    Intent placeholder = new Intent(getContext(), RecognizeTaskActivity.class);
    Bundle b = new Bundle();
    b.putInt("Offset", this.offset);
    placeholder.putExtras(b);
    ClarifaiUtil.unwrapActivity(getContext())
            .startActivity(placeholder);
  }

  private void setBusy(final boolean busy) {
    ClarifaiUtil.unwrapActivity(getContext()).runOnUiThread(new Runnable() {
      @Override
      public void run() {
        fab.setEnabled(!busy);
      }
    });
  }
  public void setAltText(String text){
    Text.setText(text);
  }

  public void setOffset(int offset){
    this.offset = offset;
  }
}

