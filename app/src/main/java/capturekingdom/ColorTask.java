package capturekingdom;

import android.content.Context;
import android.graphics.Color;

import java.util.List;

import com.clarifai.android.starter.api.v2.R;

/**
 * Created by calebkussmaul on 10/8/16.
 */

public class ColorTask implements ImageTask {

    private int colorToMatch;
    private String colorName;
    private double precisionRequired;
    private String altAdviserText;

    public ColorTask(int colorToMatch, String colorName, double precisionRequired) {
        this.colorToMatch = colorToMatch;
        this.colorName = colorName;
        this.precisionRequired = precisionRequired;
    }

    public ColorTask(int colorToMatch, String colorName, double precisionRequired, String altAdviserText) {
        this.colorToMatch = colorToMatch;
        this.colorName = colorName;
        this.precisionRequired = precisionRequired;
        this.altAdviserText = altAdviserText;
    }

    public String getAdviserText(Context ctx) {
        if(altAdviserText != null)
            return altAdviserText;
        return ctx.getString(R.string.color_task, colorName);
    }



    public boolean isValidImage(byte[] imgBytes) {

        List<Integer> colors = CKUtils.getColorsInImage(imgBytes);
        float[] hsv = new float[3];
        Color.colorToHSV(colorToMatch, hsv);

        float hueToMatch = hsv[0];

        for (int c : colors) {
            Color.colorToHSV(c, hsv);
            if(Math.abs(hueToMatch - hsv[0])/360f < precisionRequired)
                return true;
        }

        return false;
    }
}
