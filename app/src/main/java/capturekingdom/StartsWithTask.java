package capturekingdom;

import android.content.Context;
import android.util.Log;

import java.util.List;

import com.clarifai.android.starter.api.v2.R;

/**
 * Created by calebkussmaul on 10/8/16.
 */

public class StartsWithTask implements ImageTask {

    private String start;

    public StartsWithTask(String start) {
        this.start = start.toLowerCase();
    }


    public boolean isValidImage(byte[] imgBytes) {

        List<String> recognizedWords = CKUtils.getKeywords(imgBytes);

        for(int i = 0; i  < recognizedWords.size() && i < 6; i++) {
            if (recognizedWords.get(i).toLowerCase().startsWith(start))
                return true;
        }

        return false;
    }

    public String getAdviserText(Context ctx) {
        return ctx.getString(start.length() == 1 ? R.string.starts_with_task_letter : R.string.starts_with_task_word, capFirstLetter());
    }

    private String capFirstLetter() {
        if(start.length() == 0)
            return start;
        return Character.toUpperCase(start.charAt(0)) + start.substring(1);
    }
}
