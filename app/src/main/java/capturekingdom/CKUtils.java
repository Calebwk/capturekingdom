package capturekingdom;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.clarifai.android.starter.api.v2.App;

import java.util.ArrayList;
import java.util.List;

import clarifai2.api.ClarifaiResponse;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.input.image.ClarifaiImage;
import clarifai2.dto.model.Model;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Concept;
import clarifai2.dto.prediction.Prediction;


/**
 * Created by calebkussmaul on 10/8/16.
 */

public class CKUtils {

    public static List<String> getKeywords(byte[] imgBytes) {
        List<String> recognizedWords = new ArrayList<String>();

        Model model = App.get().clarifaiClient().getDefaultModels().generalModel();
        final ClarifaiResponse<List<ClarifaiOutput<Prediction>>> predictions = model.predict()
                .withInputs(ClarifaiInput.forImage(ClarifaiImage.of(imgBytes)))
                .executeSync();
        if (predictions.isSuccessful()) {
            List<ClarifaiOutput<Prediction>> response = predictions.get();

            for(Prediction keyword : response.get(0).data())
                recognizedWords.add(((Concept) keyword).name());
        }

        return recognizedWords;
    }

    public static List<Integer> getColorsInImage(byte[] imgBytes) {
        List<Integer> colors = new ArrayList<Integer>();

        Model model = App.get().clarifaiClient().getDefaultModels().colorModel();
        final ClarifaiResponse<List<ClarifaiOutput<Prediction>>> predictions = model.predict()
                .withInputs(ClarifaiInput.forImage(ClarifaiImage.of(imgBytes)))
                .executeSync();
        if (predictions.isSuccessful()) {
            List<ClarifaiOutput<Prediction>> response = predictions.get();

            for(Prediction keyword : response.get(0).data())
                colors.add(android.graphics.Color.parseColor(keyword.asColor().hex()));
        }

        return colors;
    }

    public int getCastleWallColor(int accentColor) {

        int ar = Color.red(accentColor), ag = Color.green(accentColor), ab = Color.blue(accentColor);
        return Color.rgb(ar/3, ag/3, ab/3);
    }

    public void replaceColor(Bitmap img, int c0, int c1) {
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());
        for(int i = 0; i < pixels.length; i++)
            if(pixels[i] == c0)
                pixels[i] = c1;
        img.setPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());
    }

    public Bitmap getCastle(Bitmap castleTemplate, int accentColor) {

        replaceColor(castleTemplate, 0xFFFF0000, accentColor);
        replaceColor(castleTemplate, 0xFF00FF00, getCastleWallColor(accentColor));
        return castleTemplate;
    }

    public int getMostSaturatedColor(List<Integer> colors) {
        float[] hsv = new float[3];
        float maxSat = 0f;
        int maxIdx = 0;

        for(int i = 0; i < colors.size(); i++) {
            Color.colorToHSV(colors.get(i), hsv);

            if(hsv[1] > maxSat) {
                maxSat = hsv[1];
                maxIdx = i;
            }
        }
        return colors.get(maxIdx);
    }
}
