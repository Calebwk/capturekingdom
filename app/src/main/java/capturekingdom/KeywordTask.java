package capturekingdom;

import android.content.Context;

import java.util.List;

import com.clarifai.android.starter.api.v2.R;


/**
 * Created by calebkussmaul on 10/8/16.
 */

public class KeywordTask implements ImageTask {

    private List<String> keywords;
    private String altAdviserText;

    public KeywordTask(List<String> keywords) {
        for(int i = 0; i < keywords.size(); i++)
            keywords.set(i, keywords.get(i).toLowerCase());
        this.keywords = keywords;
    }

    public KeywordTask(List<String> keywords, String adviserText) {
        this(keywords);
        this.altAdviserText = adviserText;
    }

    public String getAdviserText(Context ctx) {
        if(altAdviserText != null)
            return altAdviserText;
        return ctx.getString(R.string.default_keyword_task, keywords.get(0));
    }

    public boolean isValidImage(byte[] imgBytes) {

        List<String> recognizedWords = CKUtils.getKeywords(imgBytes);

        for(String word : recognizedWords)
            if(keywords.contains(word))
                return true;

        return false;
    }
}
