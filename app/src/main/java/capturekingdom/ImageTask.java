package capturekingdom;

import android.content.Context;

/**
 * Created by calebkussmaul on 10/8/16.
 *
 * interface for tasks that user needs to complete: Take a pic of a something blue, something that starts with 'A', etc.
 */

public interface ImageTask {
    public boolean isValidImage(byte[] imgBytes);

    public String getAdviserText(Context ctx);
}
