package capturekingdom;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.clarifai.android.starter.api.v2.R;

import java.util.List;

/**
 * Created by calebkussmaul on 10/8/16.
 */

public class ColorRecordTask implements ImageTask {

    private int colorToMatch;
    private String colorName;
    private double precisionRequired;
    private String altAdviserText;
    public int color;

    public ColorRecordTask(int colorToMatch, String colorName, double precisionRequired, String altAdviserText) {
        this.colorToMatch = colorToMatch;
        this.colorName = colorName;
        this.precisionRequired = precisionRequired;
        this.altAdviserText = altAdviserText;
    }

    public String getAdviserText(Context ctx) {
        if(altAdviserText != null)
            return altAdviserText;
        return ctx.getString(R.string.color_task, colorName);
    }


    public int getColor(){
        return this.color;
    }

    public boolean isValidImage(byte[] imgBytes) {

        List<Integer> colors = CKUtils.getColorsInImage(imgBytes);
        CKUtils util = new CKUtils();
        this.color   = util.getMostSaturatedColor(colors);
        Log.d("this color", Integer.toString(this.color));
        return true;
    }
}
