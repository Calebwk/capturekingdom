package capturekingdom;

import android.content.Context;

import com.clarifai.android.starter.api.v2.App;
import com.clarifai.android.starter.api.v2.R;

import java.util.Arrays;

/**
 * Created by calebkussmaul on 10/8/16.
 */

public class Tasks {

    private static ImageTask[] tasks;

    public static ImageTask[] getTasks() {

        if(tasks != null)
            return tasks;

        Context ctx = App.getContext();

        tasks = new ImageTask[] {
                new ColorRecordTask(0,"filler", 0.0, "I love cock"),
                new ColorTask(0xFF0000FF, "blue", .8),
                new StartsWithTask("a"),
                new KeywordTask(Arrays.asList("people", "group", "togetherness"), ctx.getString(R.string.group_photo)),

        };

        return tasks;
    }

}
